<?php

namespace Symbiont\Dipendency\Concerns;

use Symbiont\Dipendency\Contracts\Dipendable;
use Symbiont\Dipendency\Exceptions\{
    MissingBindException, MissingContractException
};
use Symbiont\Dipendency\Pendency;

trait HandlesBindables {

    protected array $bindables = [];

    public bool $return_null_unresolved = false;
    public bool $ignore_contract = false;
    public bool $ignore_global = false;

    public function bound(string $abstract): bool {
        return (! ($bound = array_key_exists($abstract, $this->bindables)) &&
            $this->ignore_global === false) ?
            Pendency::bound($abstract) :
            $bound;
    }

    public function bind(string $abstract, mixed $concrete): Dipendable {
        $this->bindables[$abstract] = $concrete;
        return $this;
    }

    public function get(string $abstract, array $args = []): mixed {
        return $this->make($abstract, $args);
    }

    public function make(string $abstract, array $args = []): mixed {
        if (! $this->bound($abstract)) {
            if($this->return_null_unresolved) {
                return null;
            }
            throw new MissingBindException($abstract);
        }

        $concrete = $this->bindables[$abstract] ?? null;
        if(! $concrete &&
            $this->ignore_global === false &&
            Pendency::bound($abstract)) {
                return Pendency::make($abstract, $args);
        }
        $context = $concrete;
        if (is_string($concrete)) {
            // we assume it's a class
            if(class_exists($concrete)) {
                $context = (new \ReflectionClass($concrete))
                    ->newInstanceArgs($args);
            }
        }

        if (is_callable($concrete)) {
            $context = call_user_func_array($concrete, $args);
        }

        if(! $this->ignore_contract &&
            (interface_exists($abstract) && is_object($context)) &&
            ! is_a($context::class, $abstract, true)) {
            throw new MissingContractException($context::class, $abstract);
        }

        return $context;
    }

    public function global(string $abstract, array $args = []) {
        try {
            return Pendency::make($abstract, $args);
        } catch(\Exception $e) {
            throw new Exceptions\MissingGlobalBindException($abstract);
        }
    }


    public function getBindings(): array {
        return $this->bindables;
    }

    public function setBindings(array $bindings): Dipendable {
        $this->bindables = $bindings;
        return $this;
    }

}