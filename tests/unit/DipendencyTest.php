<?php

namespace Symbiont\Dipendency\Tests\unit;

use PHPUnit\Framework\TestCase;

use Symbiont\Dipendency\Contracts\Dipendable;
use Symbiont\Dipendency\Dipendency;
use Symbiont\Dipendency\Exceptions\ImmutableBindAttemptException;
use Symbiont\Dipendency\Exceptions\MissingContractException;

// test interaces and classes
interface TestInterface { }
class TestClass {}
class TestClassArgs {
    public function __construct($arg) {}
}
class TestClassContracted implements TestInterface {}

final class DipendencyTest extends TestCase {

    protected Dipendable $di;
    protected $bindings;

    protected function setUp(): void {
        $this->di = new Dipendency;
        $this->di->setBindings([]);
    }

    public function testGetSet() {
        $this->assertEmpty($this->di->getBindings());

        $bindings = [
            'some' => 'test'
        ];

        $current = $this->di->getBindings();
        $this->assertIsArray($current);
        $this->assertEmpty($current);
        $this->di->setBindings($bindings);
        $this->assertEquals($bindings, $this->di->getBindings());
    }

    public function testBind() {
        $this->assertEmpty($this->di->getBindings());

        $bindings = [
            'some' => 'test'
        ];

        $this->assertEmpty($this->di->getBindings());
        $this->di->bind('some', 'test');
        $current = $this->di->getBindings();
        $this->assertEquals($bindings, $current);
    }

    public function testBound() {
        $this->assertEmpty($this->di->getBindings());

        $this->assertFalse($this->di->bound('test'));
        $this->di->bind('test', 'testing');
        $this->assertTrue($this->di->bound('test'));
        $this->assertEquals([
            'test' => 'testing'
        ], $this->di->getBindings());
        $this->di->bind('testing', null);
        $this->assertTrue($this->di->bound('testing'));
    }

    public function testMakeNoneObject() {
        $this->assertEmpty($this->di->getBindings());

        $this->di->setBindings([
            'number' => 10,
            'float' => 1.2,
            'string' => 'something',
            'null' => null,
            'array' => ['some' => 'thing']
        ]);

        $this->assertEquals(10, $this->di->get('number'));
        $this->assertEquals(1.2, $this->di->get('float'));
        $this->assertEquals('something', $this->di->get('string'));
        $this->assertEquals(null, $this->di->get('null'));
        $this->assertEquals(['some' => 'thing'], $this->di->get('array'));
    }

    public function testMakeObject() {
        $this->assertEmpty($this->di->getBindings());

        // object
        $object = new class {};
        $this->di->bind('some', $object);
        $this->assertEquals($object, $this->di->get('some'));

        // contract
        $object = new class implements TestInterface {};
        $this->di->bind(TestInterface::class, $object);
        $this->assertEquals($object, $this->di->make(TestInterface::class));
    }

    public function testMakeContract() {
        $this->assertEmpty($this->di->getBindings());

        $this->di->bind('class', TestClass::class);
        $current = $this->di->make('class');
        $this->assertIsObject($current);
        $this->assertInstanceOf(TestClass::class, $current);

        $this->di->bind(TestInterface::class, TestClassContracted::class);
        $current = $this->di->make(TestInterface::class);
        $this->assertIsObject($current);
        $this->assertInstanceOf(TestInterface::class, $current);
    }

    public function testMakeObjectMissingContract() {
        $this->assertEmpty($this->di->getBindings());

        $this->di->bind(TestInterface::class, TestClass::class);
        $this->expectException(MissingContractException::class);
        $this->di->make(TestInterface::class);

        $this->di->ignore_contract = true;
        $current = $this->di->make(TestInterface::class);
        $this->assertIsObject($current);
        $this->assertInstanceOf(TestClass::class, $current);
    }

    public function testMakeObjectArgs() {
        $this->assertEmpty($this->di->getBindings());

        $this->di->bind('class', TestClassArgs::class);
        $current = $this->di->make('class', ['some-id']);
        $this->assertIsObject($current);
        $this->assertInstanceOf(TestClassArgs::class, $current);

        $current = $this->di->make('class', ['arg' =>  'some-id']);
        $this->assertIsObject($current);
        $this->assertInstanceOf(TestClassArgs::class, $current);
    }

    public function testMakeObjectArgsMissing() {
        $this->assertEmpty($this->di->getBindings());

        $this->di->bind('class', TestClassArgs::class);
        $this->expectException(\ArgumentCountError::class);
        $this->di->make('class');
    }

    public function testMakeCallback() {
        $this->assertEmpty($this->di->getBindings());

        $this->di->bind('callback', function(): string {
            return 'string';
        });
        $this->assertEquals('string', $this->di->get('callback'));

        $object = new class {};
        $this->di->bind('callback', function() use($object): object {
            return $object;
        });
        $this->assertEquals($object, $this->di->get('callback'));
    }

    public function testMakeCallbackArgs() {
        $this->assertEmpty($this->di->getBindings());

        $this->di->bind('callback', function(string $arg) {
            return $arg . ' returned';
        });
        $this->assertEquals('string returned', $this->di->get('callback' , ['string']));
        $this->assertEquals('string returned', $this->di->get('callback' , ['arg' => 'string']));
    }

    public function testImmutables() {
        $this->assertEmpty($this->di->getBindings());

        $this->di->bind('a', 'b');
        $this->assertTrue($this->di->mutable('a'));
        $this->di->immutable('a');
        $this->assertFalse($this->di->mutable('a'));

        // $this->expectException(ImmutableBindAttemptException::class);
        $this->di->bind('a', 'c');
        $this->assertEquals('b', $this->di->make('a'));

        $this->expectException(ImmutableBindAttemptException::class);
        $this->di::$throw_exception_if_muted = true;
        $this->di->bind('a', 'c');
        $this->di::$throw_exception_if_muted = false;

        $this->di->unmute('a');
        $this->assertTrue($this->di->mutable('a'));

        $this->di->bind('a', 'c');
        $this->assertEquals('c', $this->di->make('a'));

    }
}